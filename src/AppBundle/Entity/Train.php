<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Train
 *
 * @ORM\Table(name="train")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TrainRepository")
 */
class Train
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="number", type="string", length=255, nullable=true)
     */
    private $number;

    /**
     * @var int
     *
     * @ORM\Column(name="wagons", type="integer", nullable=true)
     */
    private $wagons;

    public function __toString()
    {
        return $this->number;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set number
     *
     * @param string $number
     *
     * @return Train
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set wagons
     *
     * @param integer $wagons
     *
     * @return Train
     */
    public function setWagons($wagons)
    {
        $this->wagons = $wagons;

        return $this;
    }

    /**
     * Get wagons
     *
     * @return int
     */
    public function getWagons()
    {
        return $this->wagons;
    }
}

