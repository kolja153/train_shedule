<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Shedule
 *
 * @ORM\Table(name="shedule")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SheduleRepository")
 */
class Shedule
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_time", type="datetime")
     */
    private $time;

    /**
     * @ORM\ManyToOne(targetEntity="Station", inversedBy="firstStation")
     * @ORM\JoinColumn(name="first_station", referencedColumnName="id", onDelete="CASCADE")
     */
    private $firstStation;

    /**
     * @ORM\ManyToOne(targetEntity="Station", inversedBy="lastStation")
     * @ORM\JoinColumn(name="last_station", referencedColumnName="id", onDelete="CASCADE")
     */
    private $lastStation;

    /**
     * @ORM\ManyToOne(targetEntity="Train", inversedBy="train")
     * @ORM\JoinColumn(name="train", referencedColumnName="id",onDelete="CASCADE")
     */
    private $train;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255, nullable=true)
     */
    private $type;

    public function __construct()
    {
        $this->time = new \DateTime('now');
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set time
     *
     * @param \DateTime $time
     *
     * @return Shedule
     */
    public function setTime($time)
    {
        $this->time = $time;

        $this->setType($time);

        return $this;
    }

    /**
     * Get time
     *
     * @return \DateTime
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Set firstStation
     *
     * @param Station $firstStation
     * @return $this
     */
    public function setFirstStation(\AppBundle\Entity\Station $firstStation)
    {
        $this->firstStation = $firstStation;

        return $this;
    }

    /**
     * Get firstStation
     *
     * @return \AppBundle\Entity\Station
     */
    public function getFirstStation()
    {
        return $this->firstStation;
    }

    /**
     * Set lastStation
     *
     * @param string $lastStation
     *
     * @return Shedule
     */
    public function setLastStation($lastStation)
    {
        $this->lastStation = $lastStation;

        return $this;
    }

    /**
     * Get lastStation
     *
     * @return string
     */
    public function getLastStation()
    {
        return $this->lastStation;
    }

    /**
     * Set train
     *
     * @param string $train
     *
     * @return Shedule
     */
    public function setTrain($train)
    {
        $this->train = $train;

        return $this;
    }

    /**
     * Get train
     *
     * @return string
     */
    public function getTrain()
    {
        return $this->train;
    }

    /**
     * Set type
     *
     * @param $type
     * @return $this
     */
    public function setType($type)
    {

        $this->type = 'weekdays';

        $day = date('w', $this->time->getTimestamp());

        if ($day == 0 || $day == 6) {
            $this->type = 'weekendays';

        }

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
}

