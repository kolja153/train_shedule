<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Station;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Station controller.
 *
 * @Route("station")
 */
class StationController extends Controller
{
    /**
     * Lists all station entities.
     *
     * @Route("/", name="station_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $stations = $em->getRepository('AppBundle:Station')->findAll();

        return $this->render('station/index.html.twig', [
            'stations' => $stations,
        ]);
    }

    /**
     * Creates a new station entity.
     *
     * @Route("/new", name="station_new")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $station = new Station();
        $form = $this->createForm('AppBundle\Form\StationType', $station);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($station);
            $em->flush();

            return $this->redirectToRoute('station_show', ['id' => $station->getId()]);
        }

        return $this->render('station/new.html.twig', [
            'station' => $station,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a station entity.
     *
     * @Route("/{id}", name="station_show")
     * @Method("GET")
     *
     * @param Station $station
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Station $station)
    {
        $deleteForm = $this->createDeleteForm($station);

        return $this->render('station/show.html.twig', [
            'station' => $station,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Displays a form to edit an existing station entity.
     *
     * @Route("/{id}/edit", name="station_edit")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @param Station $station
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, Station $station)
    {
        $deleteForm = $this->createDeleteForm($station);
        $editForm = $this->createForm('AppBundle\Form\StationType', $station);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('station_edit', ['id' => $station->getId()]);
        }

        return $this->render('station/edit.html.twig', [
            'station' => $station,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Deletes a station entity.
     *
     * @Route("/{id}", name="station_delete")
     * @Method("DELETE")
     *
     * @param Request $request
     * @param Station $station
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, Station $station)
    {
        $form = $this->createDeleteForm($station);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($station);
            $em->flush();
        }

        return $this->redirectToRoute('station_index');
    }

    /**
     * Creates a form to delete a station entity.
     *
     * @param Station $station The station entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Station $station)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('station_delete', ['id' => $station->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
