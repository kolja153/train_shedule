<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Shedule;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Shedule controller.
 *
 * @Route("shedule")
 */
class SheduleController extends Controller
{
    /**
     * Lists all shedule entities.
     *
     * @Route("/", name="shedule_index")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $shedules = $em->getRepository('AppBundle:Shedule')
            ->allPaginate($order = 'DESC', $request->get('page') ?: 1);

        return $this->render('shedule/index.html.twig', [
            'shedules' => $shedules['shedules'],
            'pages' => $shedules['pages']
        ]);
    }

    /**
     * Creates a new shedule entity.
     *
     * @Route("/new", name="shedule_new")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $shedule = new Shedule();
        $form = $this->createForm('AppBundle\Form\SheduleType', $shedule);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($shedule);
            $em->flush();

            return $this->redirectToRoute('shedule_show', [
                'id' => $shedule->getId()]);
        }

        return $this->render('shedule/new.html.twig', [
            'shedule' => $shedule,
            'form' => $form->createView(),
        ]);
    }

    /**
     *Finds and displays a shedule entity.
     *
     * @Route("/{id}", name="shedule_show")
     * @Method("GET")
     *
     * @param Shedule $shedule
     * @return \Symfony\Component\HttpFoundation\Response
     */
         public function showAction(Shedule $shedule)
    {
        $deleteForm = $this->createDeleteForm($shedule);

        return $this->render('shedule/show.html.twig', [
            'shedule' => $shedule,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Displays a form to edit an existing shedule entity.
     *
     * @Route("/{id}/edit", name="shedule_edit")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @param Shedule $shedule
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, Shedule $shedule)
    {
        $deleteForm = $this->createDeleteForm($shedule);
        $editForm = $this->createForm('AppBundle\Form\SheduleType', $shedule);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('shedule_edit', ['id' => $shedule->getId()]);
        }

        return $this->render('shedule/edit.html.twig', [
            'shedule' => $shedule,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Deletes a shedule entity.
     *
     * @Route("/{id}", name="shedule_delete")
     * @Method("DELETE")
     *
     * @param Request $request
     * @param Shedule $shedule
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, Shedule $shedule)
    {
        $form = $this->createDeleteForm($shedule);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($shedule);
            $em->flush();
        }

        return $this->redirectToRoute('shedule_index');
    }

    /**
     * Creates a form to delete a shedule entity.
     *
     * @param Shedule $shedule The shedule entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Shedule $shedule)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('shedule_delete',['id' => $shedule->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
