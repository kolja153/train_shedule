<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * Index action
     *
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $stations = $em->getRepository('AppBundle:Station')->findAll();

        $shedules = $em->getRepository('AppBundle:Shedule')
            ->findBy(['firstStation' => 1], ['time' => 'ASC'], 5);

        return $this->render('default/index.html.twig', [
            'shedules' => $shedules,
            'stations' => $stations,
        ]);
    }
}
