<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class FilterController extends Controller
{
    /**
     * Get shedule's filter
     *
     * @Route("/filter", name="filter")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $first_station = $request->request->get('first_station');
        $last_station = $request->request->get('last_station');
        $type = $request->request->get('type');

        $shedules = $em->getRepository('AppBundle:Shedule')
            ->getByStation($first_station, $last_station, $type);

        return $this->render('default/filter.html.twig',
            [
                'shedules' => $shedules
            ]);
    }
}
