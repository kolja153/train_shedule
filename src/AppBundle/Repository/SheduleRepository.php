<?php

namespace AppBundle\Repository;

use Symfony\Component\Validator\Constraints\DateTime;

/**
 * SheduleRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class SheduleRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * Get shedule by stations
     *
     * @param $first_station
     * @param $last_station
     * @param $type
     * @return array
     */
    public function getByStation($first_station, $last_station, $type)
    {
        return $this->createQueryBuilder('s')
            ->select('s', 'f', 'l', 't')
            ->where('s.firstStation = :firstStation')
            ->andWhere('s.lastStation = :lastStation')
            ->andWhere('s.type = :type')
            ->andWhere('s.time >:time')
            ->leftJoin('s.firstStation', 'f')
            ->leftJoin('s.lastStation', 'l')
            ->leftJoin('s.train', 't')
            ->setParameter('firstStation', $first_station)
            ->setParameter('lastStation', $last_station)
            ->setParameter('type', $type)
            ->setParameter('time', new \DateTime('now'))
            ->getQuery()
            ->getArrayResult();
    }

    /**
     * Get shedules
     *
     * @param string $order
     * @param int $page
     * @param int $limit
     * @return array
     */
    public function allPaginate($order = 'DESC', $page = 1, $limit = 5)
    {
        return [
            'shedules' => $this->findBy([], ['id' => $order], $limit, ($page - 1) * $limit),
            'pages' => ceil(count($this->findAll()) / $limit),
        ];
    }
}
