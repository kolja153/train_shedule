$(document).ready(function ($) {
    $('#filter_form').click(function () {
        $.ajax({
            type: 'POST',
            url: '/filter',
            // dataType:'json',
            data: $('#filter_form').serialize(),
            success: function (data) {
                $('#table').html(data);
            }
        });
    })
});